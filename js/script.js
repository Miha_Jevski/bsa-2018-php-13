var users = [
    {
        "id": 1,
        "name": "Leanne Graham",
        "email": "Sincere@april.biz",
        "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg"
    },
    {
        "id": 2,
        "name": "Ervin Howell",
        "email": "Shanna@melissa.tv",
        "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg"
    },
    {
        "id": 3,
        "name": "Clementine Bauch",
        "email": "Nathan@yesenia.net",
        "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg"
    },
]

new Vue({
    el: '#app',
    data: {
        users: [],
        checkedUser: {
            id: null,
            name: '',
            email: '',
            avatar: '',
        },
        userIndex: null,
        showEditPopup: false,
        editMode: false,
        search: '',
        searchBy: 'name',
    },
    computed:{
        filteredUsers() {
            if (this.searchBy == "name") {
                return this.users.filter((element) => {
                    return element.name.toLowerCase().includes(this.search
                        .toLowerCase());
                });
            }
            if (this.searchBy == "email") {
                return this.users.filter((element) => {
                    return element.email.toLowerCase().includes(this.search
                        .toLowerCase());
                });
            }
        }
    },
    methods: {
        showCreate() {
            this.showEditPopup = true
        },
        showEdit(user, index) {
            this.userIndex = index
            Object.assign(this.checkedUser, user)
            this.showEditPopup = true
            this.editMode = true
        },
        createUser() {
            this.users.push(this.checkedUser)
            this.cleanUser()
            this.showEditPopup = false
        },
        editUser(){
            this.users[this.userIndex] = this.checkedUser
            this.cleanUser()
            this.showEditPopup = false
            this.editMode = false
            this.userIndex = null
        },
        deleteUser(id) {
            this.users = this.users.filter(user => user.id !== id)
        },
        cancelChange() {
            this.cleanUser()
            this.showEditPopup = false
            this.editMode = false
            this.userIndex = null
        },
        cleanUser() {
            this.checkedUser = {
                id: '',
                name: '',
                email: '',
                avatar: '',
            }
        }
    },
    created() {
        this.users = users;
    }
});